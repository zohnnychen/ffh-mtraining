package org.freedomfromhunger.mtraining;

import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.IOException;
import java.util.Locale;

public class LessonActivity extends FragmentActivity {

    private static final String TAG = "SplashAct";
    private static final int MSG_PAUSE = 100;

    LessonStepAdapter mLessonStepAdapter;
    ViewPager mViewPager;
    private View mButClose;
    private View mButNext;

    private MediaPlayer mPlayer;
    private Handler mHandler;
    private Lesson mLesson;
    private Locale mLocale;


    private static Locale sDefault = Locale.ENGLISH;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);

        // Get locale from calling activity. Have a fallback default
        mLocale = sDefault;
        if (getIntent().hasExtra(LessonLangActivity.ARG_LOCALE)) {
            try {
                mLocale = (Locale) getIntent().getSerializableExtra(LessonLangActivity.ARG_LOCALE);
                Log.d(TAG, "Got locale " + mLocale.getLanguage());
            } catch (Exception e) {
                mLocale = sDefault;
            }
        }

        // Hardcode to our only lesson so far
        mLesson = Lesson.sCondomLesson;
        if (mLesson.getAudioResId(mLocale) == 0) {
            Log.w(TAG, "Desired lang not supported: " + mLocale + ". Falling back to default");
            mLocale = sDefault;
        } else {
            Log.d(TAG, "Desired lang is supported : " + mLocale);
        }

        mHandler = new Handler(new Handler.Callback() {
            // Handle the "pause" message
            @Override
            public boolean handleMessage(Message message) {
                if (message.what != MSG_PAUSE) {
                    return false;
                }
                if (mPlayer != null) {
                    try {
                        if (mPlayer.isPlaying()) {
                            Log.d(TAG, "Pausing because step over : " + message.arg1);
                            mPlayer.pause();
                        }
                    } catch (IllegalStateException e) {
                        Log.w(TAG, "Player error while pausing", e);
                    }
                }
                return true;
            }
        });


        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        mLessonStepAdapter =
                new LessonStepAdapter(getSupportFragmentManager(), mLesson);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mLessonStepAdapter);

        mButClose = findViewById(R.id.button_close);
        mButNext = findViewById(R.id.button_next);
        initButtons();
        // Make visible when MediaPlayer available
        mButNext.setVisibility(View.INVISIBLE);

        asyncInitPlayer(mLocale);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mPlayer != null) {
            try {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                }
            } catch (Exception e) {
                Log.w(TAG, "Unable to release player", e);
            }
        }
    }

    private void asyncInitPlayer(final Locale desiredLocale) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                int resId = mLesson.getAudioResId(desiredLocale);
                if (resId <= 0) {
                    // default to something
                    resId = R.raw.lesson1_mossi;
                }
                mPlayer = new MediaPlayer();
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                String uri = "android.resource://" + getPackageName() + "/" + resId;
                try {
                    mPlayer.setDataSource(LessonActivity.this, Uri.parse(uri));
                } catch (IOException e) {
                    Log.w(TAG, "Unable to open player", e);
                }
                mPlayer.prepareAsync();

                mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                        Log.w(TAG, "Resetting. Got Media Player error code " + i + ", " + i1);
                        try {
                            mediaPlayer.reset();
                        } catch (Exception e) {
                            Log.w(TAG,"Unable to reset", e);
                        }
                        return false;
                    }
                });
                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        Log.d(TAG, "Player prepared ");
                        mButNext.setVisibility(View.VISIBLE);

                        // Goto first page
                        handleOnPage(0);
                    }
                });

                // Init handler that will pause playback

            }
        });
    }

    private void playAudio(final Lesson.Step step) {
        if (mPlayer == null) {
            return;
        }
        if (mPlayer != null) {
            try {
                if (mPlayer.isPlaying()) {
                    Log.d(TAG, "Interupting current playback and going to step " + step.name);
                    mPlayer.pause();
                }
            } catch (IllegalStateException e) {
                Log.w(TAG, "Player error", e);
            }
        }
        mPlayer.seekTo(step.start);
        mPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(final MediaPlayer mediaPlayer) {
                int durationMillis = step.end - step.start;
                if (durationMillis < 0) {
                    Log.w(TAG, "Encountered invalid duration, " + durationMillis + ", step " + step);
                    return;
                }
                try {
                    // Eliminate any pending pause requests
                    mHandler.removeMessages(MSG_PAUSE);

                    // Start playing
                    mediaPlayer.start();
                    Message msg = mHandler.obtainMessage(MSG_PAUSE, step.ordinal, 0);
                    Log.d(TAG, "Attempting to start at step " + step.name + ", ordinal " + msg.arg1);
                    mHandler.sendMessageDelayed(msg, durationMillis);
                } catch (IllegalStateException e) {
                    Log.w(TAG, "Player error", e);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPlayer != null) {
            mPlayer.release();
        }
    }

    private void initButtons() {
        mButClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mButNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cur = mViewPager.getCurrentItem();
                int next = (cur + 1 < mLesson.getNumSteps()) ? cur + 1 : cur; // don't exceed bounds of array
                mViewPager.setCurrentItem(next);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                handleOnPage(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void handleOnPage(int position) {
        int nextVisibility = position == mLesson.getNumSteps() - 1 ? View.INVISIBLE : View.VISIBLE;
        mButNext.setVisibility(nextVisibility);
        Log.d(TAG, "Page selected " + position);
        // Start playing media
        Lesson.Step step = mLesson.getStep(position);
        if (step != null) {
            playAudio(step);
        } else {
            Log.w(TAG, "No step found for position " + position);
        }
    }

    // Accessed by static Fragment
    Lesson getLesson() {
        return mLesson;
    }

    private static class LessonStepAdapter extends FragmentPagerAdapter {

        private final Lesson mLesson;

        public LessonStepAdapter(FragmentManager fm, Lesson lesson) {
            super(fm);
            mLesson = lesson;
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "Adapter asking for item " + position);
            return LessonStepFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return mLesson.getNumSteps();
        }
    }


    public static class LessonStepFragment extends Fragment {

        private int mPosition;

        static LessonStepFragment newInstance(int position) {
            LessonStepFragment f = new LessonStepFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("position", position);
            f.setArguments(args);

            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mPosition = getArguments() != null ? getArguments().getInt("position") : 0;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.frag_lessonstep, container, false);
            ImageView image = (ImageView) v.findViewById(R.id.imageView);

            LessonActivity act = (LessonActivity) getActivity();
            if (act == null) {
                return v;
            }
            Lesson lesson = act.getLesson();

            Bitmap bm = lesson.getImage(mPosition);
            if (bm != null) {
                image.setImageBitmap(bm);
            }
            Log.d(TAG, "For pos " + mPosition + ", got bm ");
            return v;
        }
    }
}
