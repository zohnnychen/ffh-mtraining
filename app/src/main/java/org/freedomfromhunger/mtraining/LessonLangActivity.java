package org.freedomfromhunger.mtraining;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.Locale;


public class LessonLangActivity extends Activity {

    public static final String ARG_LOCALE = "ARG_LOCALE";
    private View mEnglish;
    private View mMossi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_lang);
        mEnglish = findViewById(R.id.lang_english);
        mEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale l = Locale.ENGLISH;
                startActivity(l);
            }
        });

        mMossi = findViewById(R.id.lang_mossi);
        mMossi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale l = new Locale("mos");
                startActivity(l);
            }
        });
        mMossi.setVisibility(View.INVISIBLE);
        mEnglish.setVisibility(View.INVISIBLE);

        // Start loading right away
        Lesson.sCondomLesson.loadImageAsync(this, new Lesson.LessonCallback() {
            @Override
            public void onFinish() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMossi.setVisibility(View.VISIBLE);
                        mEnglish.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }

    private void startActivity(Locale locale) {
        Intent i = new Intent(LessonLangActivity.this, LessonActivity.class);
        i.putExtra(ARG_LOCALE, locale);
        startActivity(i);
    }

}
