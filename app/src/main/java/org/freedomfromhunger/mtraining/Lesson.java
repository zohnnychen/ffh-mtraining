package org.freedomfromhunger.mtraining;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Reference static lesson instances.  Instances need to be initialized; they start out
 * without any images loaded nor audio files associated with it.
 */
public class Lesson {

    private static final String TAG = "Lesson";
    private List<Step> mSteps = new ArrayList<>();
    private HashMap<String, Bitmap> mStep2Illustration = new HashMap<>();
    private HashMap<Locale, Integer> mLocale2AudioResId = new HashMap<>();

    private static Step[] sCondomLessonSteps = new Step[]{
            new Step("step1", 135000, 138000, R.drawable.step1, 1),
            new Step("step2", 138000, 147000, R.drawable.step2, 2),
            new Step("step3", 147000, 171000, R.drawable.step3, 3),
            new Step("step4", 171000, 203000, R.drawable.step4, 4),
            new Step("step5", 203000, 209000, R.drawable.step5, 5),
    };

    /**
     * Condom Lesson. Very short for the test
     */
    public static Lesson sCondomLesson = new Lesson(sCondomLessonSteps);

    static {
        // Mossi (or Moore) ISO 639 code is "mos". It's country code is "bf"
        sCondomLesson.addAudio(R.raw.lesson1_mossi, new Locale("mos"));

        // For english
        sCondomLesson.addAudio(R.raw.lesson1_english, Locale.ENGLISH);
    }

    public Lesson(Step[] steps) {
        Collections.addAll(mSteps, steps);
    }

    public Bitmap getImage(int position) {
        String key = mSteps.get(position).name;
        if (position >= 0 && position < mSteps.size()
                && key != null
                && mStep2Illustration.containsKey(key)) {
            return mStep2Illustration.get(key);
        }
        return null;
    }

    public int getAudioResId(Locale locale) {
        Integer res = mLocale2AudioResId.get(locale);
        if (res == null) {
            return -1;
        }
        return res;
    }

    public interface LessonCallback {
        public void onFinish();
    }

    private static final int BM_REALIZED_WIDTH = 409;
    private static final int BM_REALIZED_HEIGHT = 320;
    public void loadImageAsync(final Context context, final LessonCallback callback) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                for (Step step : mSteps) {
                    Bitmap bm = decodeSampledBitmapFromResource(context.getResources(), step.imageResId, 0, 0, 2);
                    if (bm != null) {
                        mStep2Illustration.put(step.name, bm);
                    }
                }
                if (callback != null) {
                    callback.onFinish();
                }
            }
        });
    }

    public int getNumSteps() {
        return mSteps.size();
    }

    public Step getStep(int position) {
        if (position < 0 || position >= mSteps.size()) {
            return null;
        }
        return mSteps.get(position);
    }

    public static class Step {
        public String name;
        public int start;
        public int end;
        public int imageResId;
        public int ordinal;

        public Step(String name, int start, int end, int imageResId, int ordinal) {
            this.name = name;
            this.start = start;
            this.end = end;
            this.imageResId = imageResId;
            this.ordinal = ordinal;
        }
    }

    private void addAudio(int resId, Locale locale) {
        mLocale2AudioResId.put(locale, resId);
    }


    private Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                   int reqWidth, int reqHeight, int sampleSizeOverride) {
        try {

            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            if (reqWidth > 0 && reqHeight> 0) {
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(res, resId, options);

                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            }

            if (sampleSizeOverride > 0) {
                options.inSampleSize = sampleSizeOverride;
            }

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            Bitmap bm = BitmapFactory.decodeResource(res, resId, options);
            Log.d(TAG,"Decoding w/ sample size " + options.inSampleSize + " and bytes " + bm.getByteCount());
            return bm;
        } catch (Error e) {
            Log.w(TAG, "Unable to decode bitmap", e);
            return BitmapFactory.decodeResource(res, R.drawable.lesson1);
        }
    }

    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
